# Frubox

Sitio web para el repositorio de apuntes y material de estudio de carreras de la UNSAM.

Acceder a través de:

- Dirección principal: https://repo.frubox.org
- Alternativa: https://frubox-frubox-759eb302c6c6793148f553948b657c7f74d3c258ad4889d1a.gitlab.io
- Vieja wiki (en proceso de baja): https://wiki.frubox.org/

Basado en MkDocs:

- https://squidfunk.github.io/mkdocs-material/publishing-your-site/#gitlab-pages
- https://squidfunk.github.io/mkdocs-material/creating-your-site/

Para correr el sitio y probar cambios localmente:

```bash
# Use a virtual environment.
python -m venv .venv  # Only once.
source .venv/bin/activate # Each time.

# Install mkdocs and the material theme.
pip install mkdocs-material

# Serve the site.
mkdocs serve
```
