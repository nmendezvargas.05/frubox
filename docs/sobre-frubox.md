# Sobre Frubox

:tomato: :lemon: :watermelon: :tangerine: :grapes: :avocado: :hot_pepper: :cherries: :pineapple: :kiwi: :strawberry: :apple: :blueberries: :coconut:

## ¿Qué es Frubox?

En la práctica, Frubox es un repositorio virtual de material de estudio, disponible aquí.

En la realidad, frubox es algo que ocurre cada día entre nosotros, pero un poco más *persistente*.

Frubox es:

> Un espacio digital para conservar y compartir nuestra experiencia de estudio,
>
> y que quede en el tiempo para *l@s* demás.

Es una forma de tener memoria de lo que pasó. Así, quienes vienen después, construyen sobre lo que ya hicimos: mejores maneras de aprender y (con)vivir en la Universidad, o en cualquier lado.

Está bueno porque, sin memoria, el estudiante siempre está naturalmente "de paso" y queda atrapado en los planes, y a veces en la indiferencia y ninguneo, de quienes tienen un puesto fijo en el sistema.

Frubox es un espacio virtual gratuito, basado en google drive, para compartir material de estudio de cualquier carrera y toda clase de información que aporte a la vida del estudiante. Frubox es principalmente un repositorio digital creado y mantenido por estudiantes de la Unsam desde el 2012, que da acceso **libre** a apuntes, exámenes, problemas resueltos y bibliografía.

Funciona así:

![20211107-194040.jpg](./img/20211107-194040.jpg)

Si bien así es como empieza, cada vez suma más funciones. Frubox ahora tiene un [directorio](/contactos) de mails para contactar a los docentes, una versión en línea del [calendario académico](/calendario) oficial, y planeamos agregar descripciones para cada materia, consejos y experiencias de cursada que nos vayan mandando.

La idea es usar y hacer crecer este espacio, sumar gente nueva, **expandirlo a otras escuelas **de la facultad y lo que se les ocurra! Si te ayudó, ayudá a que crezca, la base de este proyecto es la colaboración entre los estudiantes.

## Un espacio para compartir

Intentamos nuclear a los estudiantes para que su experiencia se transmita, y existen otros espacios con el formato de foro (unsamforo.com.ar) y muchos grupos de facebook donde también se comparte material. Por ahora este es el repositiorio más completo y barato (es gratis) donde podés conseguir todo lo que la gente (y ud.) se anime a compartir.

Para enviar algo, podés enviar un mail a <unsam@frubox.org> o subirlo a tu Google Drive y compartilo con nosotros a esa cuenta. Si algún estudiante quisiera administrar una sección, es bienvenido a sumarse al equipo.

## Perspectivas

Podríamos, por ejemplo, ser una editorial de apuntes, dar un lugar para organizar juntadas de estudio, difundir los trabajos científicos de los estudiantes avanzados, armar talleres de orientación y formación científica. Pero todo esto es a futuro, y dependerá de la iniciativa de los estudiantes, ya sea aquí o en cualquier otro espacio de la Unsam.

En la lista de deseos se encuentra tener una página de internet donde los estudiantes puedan subir cosas directamente, puedan valorar y recomendar apuntes, organizar reuniones de estudio, conectarse con estudiantes avanzados, y hacer toda clase de preguntas sobre la vida Universitaria en la Unsam. Pero para eso Frubox necesita la ayuda de manos más habilidosas.

En algun momento pensamos usar este tipo de cosas para preguntas y respuestas. O para preguntas de guias de problemas o TPs. Si llegaste hasta acá y te gustó la idea, te invitamos a sumarte al proyecto. Escribinos a <unsam@frubox.org> sin vueltas ;)

Queremos que Frubox sea todo eso, a la vez reflejando la identidad de los estudiantes. Tenemos iniciativas e ideas propias, producimos valor y queremos la mejor educación que la Unsam pueda darnos. Y quedó claro, para quién escribe, que para cambiar nuestra realidad el cambio tenía que venir de nosotros y a la vez para nosotros.

## Google Drive

*Trabajo colaborativo en línea* ¿Qué es Google Drive? Es un sitio de alojamiento de archivos en *la nube* que ofrece al mismo tiempo la posibilidad de abrir y editar esos archivos: documentos tipo word, hojas de cálculo tipo excel, presentaciones de diapositivas, PDF's, fotos, videos, audio y muchas cosas más.

Además, los archivos y carpetas se pueden compartir con otros usuarios, y editar varios a la vez el mismo documento, por ejemplo, un trabajo práctico, evitando totalmente la necesidad de ir pasándose .docx por facebook y ofrece muchas de las prestaciones que necesitamos en la Unsam para escribir informes.

## Opinión sobre problemas resueltos

Hace poco nos comentaron que hay docentes que no están a favor de subir ejercicios resueltos a Frubox. El argumento es súmamente váildo, los profesores tienen la tarea de dar el contenido de sus materias como mejor les parece, y estamos dispuestos a evaluar solicitudes de los docentes respecto del contenido que existe. Por ejemplo si contuviera errores o atentara  a la educación del estudiante claramente.

Hemos escuchado argumentar que la disponibilidad de ejercicios resueltos permite al estudiante esquivar el proceso de aprendizaje y aprobar con atajos. El contraargumento a esto es que a fin de cuentas cada uno es dueño de cómo quiere estudiar. Incluso a pesar de las expectativas del docente o del profesor particular, un gran número de alumnos quiere aprobar más de lo que quiere aprender. Nosotros no alentarmos esta idea, pero respetamos la decisión del estudiante que desea atravesar su carrera de la forma que elija.

Por otro lado, los ejercicios resueltos también pueden servir para comprender profundamente lo que se está haciendo. Este es el caso cuando están acompañados de buenas explicaciones y cuando las evaluaciones incluyen ejercicios que evalúen esa comprensión.

Así que retrucamos a este argumento pidiendo que en vez de estar en contra de tener ejercicios resueltos, hagan mejores evaluaciones. El tema está abierto a discusión.
