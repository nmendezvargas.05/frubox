# Calendario académico 📅

# 2024

📎 [ECyT 2024](https://www.unsam.edu.ar/escuelas/ecyt/archivos/Calendario2024%20-%20ECyT%20%201.0.pdf)

📎 [EH 2024](https://noticias.unsam.edu.ar/wp-content/uploads/2023/12/Calendario-Academico-2024.pdf)

📎[EEyN 2024](https://www.unsam.edu.ar/escuelas/eeyn/archivo/CalendarioAcademico2024-EEyN.pdf)

📎 [EAyP 2024](https://www.unsam.edu.ar/escuelas/eayp/docs/Calendario-2024.pdf)

📎 [EIDAES 2024](https://www.unsam.edu.ar/escuelas/eidaes/calendario.php)

📎 [EHyS 2024](https://www.unsam.edu.ar/escuelas/ehys/calendario.php)

📎 [ICRM 2024](https://www.unsam.edu.ar/institutos/icrm/docs/CA%20ICRM%202024.pdf)

---
💡 Hay carreras que aun no publican los calendarios, en caso de que quieras agregar/modificar alguno podes escribirnos a <unsam@frubox.org>.
