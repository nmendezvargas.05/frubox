# Educación Física - Instituto ciencias de la rehabilitación y el movimiento

He aquí la sección para Educación Física del Instituto ciencias de la rehabilitación y el movimiento

???+ question "No recibimos aportes para esta sección hasta ahora."

    Escribinos a <unsam@frubox.org> con tus aportes para inaugurarla. 😊

    :tomato: :lemon: :watermelon: :tangerine: :grapes: :avocado: :hot_pepper: :cherries: :pineapple: :kiwi: :strawberry: :apple: :blueberries: :coconut:

    Si tenés tiempo para escribir una descripción y pasar información relevante, podríamos publicar por acá:

    -   Opiniones, reseñas, comparaciones con otras carreras/universidades.
    -   Planes de estudio y recomendaciones para organizar los cuatrimestres.
    -   Respuestas a preguntas frecuentes o importantes: ¿Se puede cursar de noche? por ejemplo.

    Envianos recomendaciones y opiniones sobre los temas, las clases y el cuerpo docente. Todo lo que pueda ayudar a los futuros alumnos a tener una mejor cursada, sirve. Escribinos con tu aporte a <unsam@frubox.org>.

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)

## 💭 Experiencias

✏️ Escribinos con tu experiencia a <unsam@frubox.com> y lo agregamos acá.

## 📱 Grupos de whatsapp

💬 **-**

## 📂 Carpeta Frubox

> Escribinos con material para publicar acá al mail de frubox: <unsam@frubox.org>
