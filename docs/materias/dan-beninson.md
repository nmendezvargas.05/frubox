# Dan Beninson

Resúmenes de las materias que da el instituto Dan Beninson para sacar la Licencia de operación de reactores nucleares. 

Les sirve a los estudiantes de Ingeniería Nuclear del Instituto Dan Beninson y a los estudiantes de Física Médica

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)

## 💭 Experiencias

✏️ Escribinos con tu experiencia a <unsam@frubox.com> y lo agregamos acá.

## 📱 Grupos de whatsapp

💬 **-**

## 📂 Carpeta Frubox

**Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer)**

 

<iframe src="https://drive.google.com/embeddedfolderview?id=1myl91RlOf-wjNKq83KTa-tXBgffu0QwF" style="width:100%; height:600px; border:0;"></iframe>
