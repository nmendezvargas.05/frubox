# Contaminación de Aguas y Suelos

???+ question "Esta materia no tiene descripción"

    Si tenés tiempo para escribir una descripción y pasar información relevante, podríamos publicar por acá:

    -   Opiniones, reseñas, comparaciones con otras carreras/universidades.
    -   Planes de estudio y recomendaciones para organizar los cuatrimestres.
    -   Respuestas a preguntas frecuentes o importantes: ¿Se puede cursar de noche? por ejemplo.

    Envianos recomendaciones y opiniones sobre los temas, las clases y el cuerpo docente. Todo lo que pueda ayudar a los futuros alumnos a tener una mejor cursada, sirve. Escribinos con tu aporte a <unsam@frubox.org>.

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)

## 💭 Experiencias

Es una linda materia que sirve para la carrera, los contenidos están buenos, la única queja que haría yo, que la comparten mis compañeros de camada también, es que es muy diversa a lo largo del cuatrimestre.

La materia la dan tres profesores, y su forma de dar las clases varia, el primero es Candal que toma el primer parcial y explica química del agua ( da cuatro horas seguidas de cursada), luego sigue Hugo ( un genio!, el da muchas menos horas de cursada y explica el transporte de contaminantes en cuerpos de agua, una versión un poco mas fácil que fenómenos de transporte) y la tercera parte la a Curutchet( que explica suelos en DOS CLASES, de una hora y media cada una, se pueden imaginar que vemos todo muuuuy por encima)

El segundo parcial consta de una parte de Hugo y otra de Curutchet, se aprueban por separado, es decir, como si la materia tuviera tres parciales, podes aprobar uno y desaprobar el otro.

Hay una instancia de recuperación al final del cuatri donde podes dar los tres parciales.

También hay 4 laboratorios


✏️ Escribinos con tu aporte a <unsam@frubox.com> y lo agregamos acá.


## 📱 Grupos de whatsapp

💬 **-**


## 📂 Carpeta Frubox

 **Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer).**

<iframe src="https://drive.google.com/embeddedfolderview?id=1rX8wIFLvlq76khQtRbe_kjG6gmT-NjbO" style="width:100%; height:600px; border:0;"></iframe>
