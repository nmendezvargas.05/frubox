# Ecología

Gracias al enorme aporte de un compañero, abrimos esta sección para la materia Ecología (CB25) de la carrera de Ingeniería Ambiental de la Unsam. Ty!

???+ question "Esta materia no tiene descripción"

    Si tenés tiempo para escribir una descripción y pasar información relevante, podríamos publicar por acá:

    -   Opiniones, reseñas, comparaciones con otras carreras/universidades.
    -   Planes de estudio y recomendaciones para organizar los cuatrimestres.
    -   Respuestas a preguntas frecuentes o importantes: ¿Se puede cursar de noche? por ejemplo.

    Envianos recomendaciones y opiniones sobre los temas, las clases y el cuerpo docente. Todo lo que pueda ayudar a los futuros alumnos a tener una mejor cursada, sirve. Escribinos con tu aporte a <unsam@frubox.org>.

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)


## 💭 Experiencias

Los temas de la materia no son particularmente complejos, para nada; hay mucha bibliografía para quien quiera ahondar en los temas también. Especialmente en el comienzo de la materia la demanda de "trabajitos" "TPs" o "monografías" (como se los quiera llamar) es particularmente alta y muy demandante. Cuando digo "alta" me refiero a pedir 4 trabajos diferentes de aprox dos carillas, con fuentes serias citadas en la primera clase.

Tiene una salida de campo al río Reconquista y otra a Ciudad Evita, y algo de trabajo de laboratorio.

*Presencial obligatoria, promocionable.*

✏️ Escribinos con tu aporte a <unsam@frubox.com> y lo agregamos acá.


## 📱 Grupos de whatsapp

💬 **-**


## 📂 Carpeta Frubox


<iframe src="https://drive.google.com/embeddedfolderview?id=12Y2uOt21hQxXcta9x4KqbSW6m6M3nch1" style="width:100%; height:600px; border:0;"></iframe>
