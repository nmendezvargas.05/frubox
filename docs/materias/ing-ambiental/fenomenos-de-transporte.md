# Fenómenos de Transporte

Carrera: Ingeniería Ambiental, Ingeniería en Energía.

???+ question "Esta materia no tiene descripción"

    Si tenés tiempo para escribir una descripción y pasar información relevante, podríamos publicar por acá:

    -   Opiniones, reseñas, comparaciones con otras carreras/universidades.
    -   Planes de estudio y recomendaciones para organizar los cuatrimestres.
    -   Respuestas a preguntas frecuentes o importantes: ¿Se puede cursar de noche? por ejemplo.

    Envianos recomendaciones y opiniones sobre los temas, las clases y el cuerpo docente. Todo lo que pueda ayudar a los futuros alumnos a tener una mejor cursada, sirve. Escribinos con tu aporte a <unsam@frubox.org>.

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)

## 💭 Experiencias

La materia es muy linda y tambien muy complicada. En eso es parecida a Física 3. Las dos profesoras son recontra didácticas, y la ayudante contesta los mails muy bien y al toque. La modalidad de cursada te hace llevarla al día porque tiene muchos TPs y cuestionarios on line. \
Se recomienda no combinarla con otras materias jodidas.

✏️ Escribinos con tu aporte a <unsam@frubox.com> y lo agregamos acá.


## 📱 Grupos de whatsapp

💬 **-**

## 📂 Carpeta Frubox

Gracias al aporte enorme de *Anto*, acá hay apuntes, resoluciones y material cultural de altísima prolijidad.


<iframe src="https://drive.google.com/embeddedfolderview?id=1otFOmA0MI06eY5_ktORYWKCfNTiNlVMA" style="width:100%; height:600px; border:0;"></iframe>
