# Normativa Ambiental

???+ question "Esta materia no tiene descripción"

    Si tenés tiempo para escribir una descripción y pasar información relevante, podríamos publicar por acá:

    -   Opiniones, reseñas, comparaciones con otras carreras/universidades.
    -   Planes de estudio y recomendaciones para organizar los cuatrimestres.
    -   Respuestas a preguntas frecuentes o importantes: ¿Se puede cursar de noche? por ejemplo.

    Envianos recomendaciones y opiniones sobre los temas, las clases y el cuerpo docente. Todo lo que pueda ayudar a los futuros alumnos a tener una mejor cursada, sirve. Escribinos con tu aporte a <unsam@frubox.org>.

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)


## 💭 Experiencias

La materia la dan dos abogadas dedicadas al tema ambiental (Y SE NOTA!). Pretenden que entendamos su lenguaje y a veces nos trataban de tontos. 

Algunas clases tenían 150 diapositivas con puro texto y eran un embole.

En general la materia está buena, pero la forma de evaluar a veces es tediosa, muy de memoria, cosa que no estamos acostumbrados!

En un momento de la cursada tuvimos que hacer una audiencia apoyando a diferentes partes acerca de una problemática ambiental, como fue la instalación de una minera y estuvo muy bueno.

La buena noticia es que... es una materia  muy accesible a ser promocionada!


✏️ Escribinos con tu aporte a <unsam@frubox.com> y lo agregamos acá.

## 📱 Grupos de whatsapp

💬 **-**

## 📂 Carpeta Frubox

**Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer).**


<iframe src="https://drive.google.com/embeddedfolderview?id=1kBrng-fN1qcSrjSwHWrd_EDtKj15ts09" style="width:100%; height:600px; border:0;"></iframe>
