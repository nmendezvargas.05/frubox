# Energía

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)


## 💡 Acerca de la materia

Carrera similar a una ingenieria industrial, pero con un foco en la generación, el transporte, y el consumo eficiente y racional de la energía.

Tenemos una rama de materias petroleras, otra rama de conocimientos eléctricos que se empalman muy bien con las energías renovables.

Además, tenemos materias de gestión y planificación.

## 💭 Experiencias

✏️ Escribinos con tu experiencia a <unsam@frubox.com> y lo agregamos acá.


## 📱 Grupo de whatsapp

Grupo de whatsapp (Independiente de Frubox): <https://chat.whatsapp.com/IJsfnPuWQRB3WDAsGJPzVZ>

## 🗺️ Mapa de correlativas

![correlativas](./img/arbol_de_correlatividades_ing._energias-1_1.jpg)


## 📂 Carpeta en Drive

**Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer)**


<iframe src="https://drive.google.com/embeddedfolderview?id=1DKdkSsxxOf1zwOR23toRBqEWYKFR9yK_" style="width:100%; height:600px; border:0;"></iframe>
