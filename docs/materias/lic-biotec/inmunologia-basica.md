# Inmunologia Basica

???+ question "Esta materia no tiene descripción"

    Si tenés tiempo para escribir una descripción y pasar información relevante, podríamos publicar por acá:

    -   Opiniones, reseñas, comparaciones con otras carreras/universidades.
    -   Planes de estudio y recomendaciones para organizar los cuatrimestres.
    -   Respuestas a preguntas frecuentes o importantes: ¿Se puede cursar de noche? por ejemplo.

    Envianos recomendaciones y opiniones sobre los temas, las clases y el cuerpo docente. Todo lo que pueda ayudar a los futuros alumnos a tener una mejor cursada, sirve. Escribinos con tu aporte a <unsam@frubox.org>.

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)


## 💭 Experiencias

-   La materia estuvo buena y el contenido fue bastante interesante, la profesora da muy buenas teóricas.

-   Prestando atención a las clases, consultando dudas, tomando apuntes y haciendo los choice de repaso no me hizo falta ir al libro más que un par de veces (Recomiendo el Geffner como material de consulta).

-   La materia requiere mucha constancia para poder seguirla porque cada clase se integra con la anterior, así que no se dejen estar y antes de cada clase denle un repasito a la anterior.

-   Lo más pesado de la materia son los papers que hay que leer (1 por semana), resolver un parcialito y despues unos ejercicios, cosa que lleva su tiempo. Rrecomiendo hacer esas actividades en grupo.

-   Los exámenes son un par de ejercicios tipo paper y ejercicios teóricos normales, no son super dificiles pero son largos y hay que tener todo bastante claro para aprobarlos.

✏️ Escribinos con tu experiencia a <unsam@frubox.com> y lo agregamos acá.


## 📱 Grupos de whatsapp

💬 **-**

## 📂 Carpeta Frubox

**Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer)**

 
<iframe src="https://drive.google.com/embeddedfolderview?id=1ZxPHain5oEPsxGaZVqf8NpHwTdugRDOr" style="width:100%; height:600px; border:0;"></iframe>
