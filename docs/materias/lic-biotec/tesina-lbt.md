# Tesina LBT

???+ question "Esta materia no tiene descripción"

    Si tenés tiempo para escribir una descripción y pasar información relevante, podríamos publicar por acá:

    -   Opiniones, reseñas, comparaciones con otras carreras/universidades.
    -   Planes de estudio y recomendaciones para organizar los cuatrimestres.
    -   Respuestas a preguntas frecuentes o importantes: ¿Se puede cursar de noche? por ejemplo.

    Envianos recomendaciones y opiniones sobre los temas, las clases y el cuerpo docente. Todo lo que pueda ayudar a los futuros alumnos a tener una mejor cursada, sirve. Escribinos con tu aporte a <unsam@frubox.org>.

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)

## 💭 Experiencias

El trabajo de tésis de lincenciatura es obligatorio para optar por el título de licenciadx en biotecnología. Empezar una tesina abre puertas a pedir becas de trabajo científico y a aprender muchas cosas.

### Consejos:

-   Es una materia, no dejes que te maltraten. Vos estás trabajando gratis para el laboratorio y a cambio tienen que enseñarte a hacerlo bien. El maltrato es abuso.

-   Hay muchos lugares muy copados que te enseñarían los mismo o mejor. No te quedes solo en la UNSAM, podés buscar en cualquier lugar: UBA Exactas, FFyB, Leloir, IByME, INTI, INTA y no sé cuantos más. Solo enviá un correo a la gente del laboratorio que te cope y listo.

-   Algunos investigadores pretenden que te compromentas a hacer un doctorado en ese mismo labo. Es moralmente incorrecto obligarte a hacer esto, y es bueno hablarlo siempre de primera. Muchos investigadores entienden y muchos otros no (y después se enojan como niños).


### Las tres patas (+1) de elegir un laboratorio

[Abrir *Documento*](http://docs.google.com/document/edit?hgd=1&id=1Bh_MD7JjuEwUBpg4_xZBQ9i3eIDEWVUIw7PprEbaTtQ)


✏️ Escribinos con tu experiencia a <unsam@frubox.com> y lo agregamos acá.

## 📱 Grupos de whatsapp

💬 **-**

## 📂 Carpeta Frubox

**Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer)**
 

<iframe src="https://drive.google.com/embeddedfolderview?id=1j56PzSLllKhENvKWutjiu55UzGeEfk7z" style="width:100%; height:600px; border:0;"></iframe>
