# Química Biológica 2 (QB2)

???+ question "Nota sobre el plan viejo"

    ¡Hola, unsamers!
    
    Ahora nos encontramos cursando el plan LBT-2019B, por lo que tenemos a QB separada en dos materias (por fortuna). Sin embargo, en el plan viejo QB1 y QB2 estaban juntas. En la carpeta "Química Biológica (plan viejo)" hay un montón de parciales y finales viejos que aún sirven (suelen preguntar lo mismo, es lo lindo de las materias teóricas :p), por lo que les recomendamos que chusmeen ahí también para estudiar :)
    
    Un saludo desde Frubox <3

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)

## 💭 Experiencias

✏️ Escribinos con tu experiencia a <unsam@frubox.com> y lo agregamos acá.

## 📱 Grupos de whatsapp

💬  [Química Biológica](https://chat.whatsapp.com/FByDAQXXHBG1kyNSaqUl4C)

## 📂 Carpeta Frubox

**Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer)**
 

<iframe src="https://drive.google.com/embeddedfolderview?id=1NWyr09u8uGybUL4UphLNamknhUF8lgHg" style="width:100%; height:600px; border:0;"></iframe>
