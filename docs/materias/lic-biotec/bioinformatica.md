# Bioinformática

## 💡 Acerca de la materia

Bienvenidos a Bioinformática. Atrás quedó el lamentarse por tener que hacer un BLAST y que se corte internet. Olvídense de las clases de BI de otras materias o de como armar un primer, la posta está acá.

### ¿Cómo es la materia?

-   Es sencillo de aprobar pero difícil de promocionar (tranqui, la gran mayoria promociona pero completar bien el TP puede llevar un fin de semana sin dormir). 
-   La evaluación incluye un examen parcial al final de la materia. Parece eterno de lo largo que es, pero es súper claro y completo si resolviste los TPs. En otras palabras, no es dificil pero sí bastante largo.
- Tiene más clases prácticas que teóricas, y algunos TPs en el camino.
-   Tener algun conocimiento previo de programación ayuda aunque no sea esencial. En la materia van a programar en R y en la consola de comandos de Linux (a.k.a. bash).
-   La mayoría de las clases teóricas las da Fernán Agüero, pero también van a tener a Lucía Chemes y a Morten Nielsen (sus clases son en inglés, aunque él sepa hablar español).

### Lo que deja afuera

Por bioinformática se entiende una disciplina de manejo de bases de datos relativamente grandes, con información generalmente "genómica", proteómica, o de cualquier "ómica".

La bioninformática es solo una parte de la [biología computacional](https://en.wikipedia.org/wiki/Computational_biology#Bioinformatics), que incluye disciplinas relevantes que apenas se mencionan en LBT. En particular:

-   Biología de sistemas
-   Análisis de flujos metabólicos
-   Modelado de proteínas
-   Análisis de redes biológicas
-   Aprendizaje automático y "deep" learning.
-   ...

Muchos de estos temas se dan en otras universidades, y vale la pena considerarlas como optativas.

### Un par de consejos

-   La linea de comandos es intimidante al principio pero aprendan a usarla y sus vidas van a ser mucho mas felices.
-   Tambien pueden usar servicios web para muchas de las actividades (vease la lista abajo). Tengan en cuenta que les gusta fallar el dia anterior a la entrega del TP.

Enlaces útiles:

- Búsqueda de ORFs                    <https://www.ncbi.nlm.nih.gov/orffinder/>
- Búsqueda de secuencias homólogas    <https://blast.ncbi.nlm.nih.gov/Blast.cgi>
- Alineamientos múltiples             <https://www.genome.jp/tools-bin/clustalw>
- Creación de árbol filogenético     <http://iqtree.cibiv.univie.ac.at/>
- Creación de HMM                     <https://www.ebi.ac.uk/Tools/hmmer/search/hmmsearch>

Si tienen problemas con algun programa o comando y no hay un profesor a mano pueden usar:

-   Google (DuckDuckGo si son pillos)
-   En la terminal de Linux, cosas del estilo `man comando`, `comando -h`, o `comando --help` sirven para ver las instrucciones del comando con el que tengan problemas.
-   Siempre pueden preguntarle a un compa de clase o a sus amigos de Frubox!


## 💭 Experiencias

✏️ Escribinos con tu experiencia a <unsam@frubox.com> y lo agregamos acá.


## 📱 Grupos de whatsapp

💬 [Intro a la bioinformática](https://chat.whatsapp.com/CDWFzeKDmeY2CmOXqDt7OX)


## 📂 Carpeta Frubox

<iframe src="https://drive.google.com/embeddedfolderview?id=1cimNb7ifpaVwEf2XeEaYtUUbhgv34ggy" style="width:100%; height:600px; border:0;"></iframe>

## Memes

![sudovacc.jpeg](/frubox/frubox-unsam-doku/home/lic-biotec/sudovacc.jpeg)

> Si entendes el chiste ya aprobaste.
