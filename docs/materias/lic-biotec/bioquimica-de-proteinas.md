# Bioquímica de Proteínas

???+ question "Esta materia no tiene descripción"

    Si tenés tiempo para escribir una descripción y pasar información relevante, podríamos publicar por acá:

    -   Opiniones, reseñas, comparaciones con otras carreras/universidades.
    -   Planes de estudio y recomendaciones para organizar los cuatrimestres.
    -   Respuestas a preguntas frecuentes o importantes: ¿Se puede cursar de noche? por ejemplo.

    Envianos recomendaciones y opiniones sobre los temas, las clases y el cuerpo docente. Todo lo que pueda ayudar a los futuros alumnos a tener una mejor cursada, sirve. Escribinos con tu aporte a <unsam@frubox.org>.

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)


## 🗃️ Otros recursos

Presentaciones Power Point del libro de Bioquímica de Lehninger: "Principios de Bioquímica". David L. Nelson y Michael M. Cox.

-   <http://laguna.fmedic.unam.mx/lenpres/>

Exámenes kilométricos de contenido muy muy muy útil, pero fáciles. Lleve una BIC nueva "cuando se les acabe, pueden dejar de escribir". 😉

Quiza sirvan estos cheatsheets:

-   <https://thebumblingbiochemist.com/graphics/>


## 💭 Experiencias

✏️ Escribinos con tu aporte a <unsam@frubox.com> y lo agregamos acá.

## 📱 Grupos de whatsapp

💬 **-**

## 📂 Carpeta Frubox

**Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer)**
 

<iframe src="https://drive.google.com/embeddedfolderview?id=1BdeGQ-PL0h9iuIZhD9AloQjqkA2gWuRB" style="width:100%; height:600px; border:0;"></iframe>
