# Inmunología Molecular (II)

???+ question "Esta materia no tiene descripción"

    Si tenés tiempo para escribir una descripción y pasar información relevante, podríamos publicar por acá:

    -   Opiniones, reseñas, comparaciones con otras carreras/universidades.
    -   Planes de estudio y recomendaciones para organizar los cuatrimestres.
    -   Respuestas a preguntas frecuentes o importantes: ¿Se puede cursar de noche? por ejemplo.

    Envianos recomendaciones y opiniones sobre los temas, las clases y el cuerpo docente. Todo lo que pueda ayudar a los futuros alumnos a tener una mejor cursada, sirve. Escribinos con tu aporte a <unsam@frubox.org>.

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)



## 💭 Experiencias

Hermosa materia. En 2014 fue un refuerzo muy necesario y completo con cosas nuevas y mucha práctica. Los docentes son sensacionales, una materia para aprender muchísimo de ellos y de los temas que dan.

Acá **reclutan** trabajadores para hacer la tesina todos los años, ojo te puede gustar o no.

✏️ Escribinos con tu experiencia a <unsam@frubox.com> y lo agregamos acá.

## 📱 Grupos de whatsapp

💬 **-**

## 📂 Carpeta Frubox

**Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer)**

<iframe src="https://drive.google.com/embeddedfolderview?id=12nY9xNis59Dl00ui3_h4FPbuRyTEYPlU" style="width:100%; height:600px; border:0;"></iframe>
