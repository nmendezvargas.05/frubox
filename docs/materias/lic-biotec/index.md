# Lic. Biotecnología

## 💡 Acerca de la materia

Es difícil saber cuándo uno empieza a sentir que lo que aprende tiene algo que ver con ser licenciadx o ingenierx. Pero en general coincide con dejar de a poco ese pelotudeo bello y vida hermosa en el pasto y las escaleras del tornavías, para pasar a otro edificio y disfrutar de otro pasto y otras escaleras a menos de 100 metros.

En el caso de LBT, esto implica directamente empezar a cursar en el Insituto de Investigaciones Biotecnológicas. Por lo tanto, tomamos el edificio de cursada como criterio para diferenciar básico vs superior, ponderado con el año de cursada. Da la casualidad, o no, de que es también cuando empezamos a tener clase con investigadores del instituto y algunos de ellos empiezan a vernos como material de tesinista...

Como ejemplo del buen criterio que manejamos, Física 4 está acá, porque claramente no es básica, aunque no se curse en el IIB.


## 🗺️ Mapa de correlativas

### Árbol de Correlatividades LBT

Hay un plan nuevo que se presentó en Junio de 2018 a acreditación, por lo que está pendiente a aprobación. Dejamos enlaces para la propuesta de plan de estudios y de transición.

Nuevos esquemas: 2009 vs 2018 (vs 2019?)

### Diagrama a mano

*Eres arte*:

![pulpo_biotec.png](/materias/img/pulpo_biotec.png)

### Diagrama detallado por cuatrimestre

**Versión editable de alta resolución**: <https://docs.google.com/drawings/d/10Dzh5k9BGhJc6e79J89wEeiHnNUeJkUDx2a5INWRf7E/edit>

![correlatividades-2009-2018.png](/materias/img/correlatividades-2009-2018.png)


## 💭 Experiencias

✏️ Escribinos con tu experiencia a <unsam@frubox.com> y lo agregamos acá.

## 📱 Grupos de whatsapp

💬 [Biotecnología - UNSAM](https://chat.whatsapp.com/IyI3Pco1rElCviAHHmCHlE)
💬 [Biotech](https://chat.whatsapp.com/F47JAqdPAx2FIBbvgGj0ar)
