# Ing. Electronica y Telecom

???+ question "Esta materia no tiene descripción"

    Si tenés tiempo para escribir una descripción y pasar información relevante, podríamos publicar por acá:

    -   Opiniones, reseñas, comparaciones con otras carreras/universidades.
    -   Planes de estudio y recomendaciones para organizar los cuatrimestres.
    -   Respuestas a preguntas frecuentes o importantes: ¿Se puede cursar de noche? por ejemplo.

    Envianos recomendaciones y opiniones sobre los temas, las clases y el cuerpo docente. Todo lo que pueda ayudar a los futuros alumnos a tener una mejor cursada, sirve. Escribinos con tu aporte a <unsam@frubox.org>.

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)

## 💭 Experiencias

✏️ Escribinos con tu experiencia a <unsam@frubox.com> y lo agregamos acá.


## 📱 Grupos de whatsapp

[Ing. Telecomunicaciones](https://chat.whatsapp.com/BqJloZ2cPm1CLXxNXk5tOb)

[Ing. Electronica](https://chat.whatsapp.com/FyCfxZCmH426APjWX3sKpa)

## 🗺️ Mapa de correlativas

[Ing. Telecomunicaciones](https://drive.google.com/file/d/1jFdTND4dBJQHZ_vAeLojoeRx9D0Z1fpY/view?usp=sharing)

[Ing. Electronica](https://drive.google.com/file/d/11mqtGNU0ZGHXsLHibJoXxiAb1rr9FUKp/view?usp=sharing)



## 📂 Carpeta en Drive

Todas las materias de ingeniería electrónica (y más) organizadas y subidas por Misael. 

<iframe src="https://drive.google.com/embeddedfolderview?id=1lheMVdA5waFFfCNT6pvAc0qvUukA1Xp1" style="width:100%; height:600px; border:0;"></iframe>

Monumento a personas como esa por favor.
