# Examen de Inglés

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)


## 💭 Experiencias

> No se si sirve mi experiencia... Yo hice hasta el nivel 6 de inglés en el Programa de Lenguas de la Unsam, en el Dpto de alumnos me dijeron que justo para Ing. Ambiental no era válido. Hice el examen y cuando salí de rendir me crucé con una organizadora del Examen y me dijo que si servía así que lo hice en vano.. 
> 
> De todas formas es bastante fácil , te dejan tener diccionario Español-Inglés.
> 
> Les adjunto un modelo que me pasaron, sin embargo fue más fácil que este y el que me tomaron a mi es el otro que ya está subido en Frubox.
> 
> Posible Descripción: El examen consiste en entender un texto y responder preguntas sobre el mismo. No es muy complicado. Claro depende del nivel que cada uno maneje pero con un nivel básico es más que suficiente. Te tenes que inscribir en el Depto de Alumnos. Las fechas las van organizando no es siempre igual y depende de la cantidad de gente que se anote. La mejor opción es ir preguntando en Alumnos. En 2019 se permitio tener diccionario Español - Inglés. 
> 
> Opcional: No quiero decir algo general y que después no sea pero si mal no recuerdo podias responder en Español.


✏️ Escribinos con tu experiencia a <unsam@frubox.com> y lo agregamos acá.

## 📱 Grupos de whatsapp

💬 **-**

## 📂 Carpeta Frubox

**Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer)**

<iframe src="https://drive.google.com/embeddedfolderview?id=1RB6gWEOwdsKV8SwBugPhpPSQRUe9NIPQ" style="width:100%; height:600px; border:0;"></iframe>
