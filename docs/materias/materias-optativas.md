# Materias Optativas


## 💭 Experiencias

Experiencias anteriores, material de cursada y formulario de materias optativas que se encontraron.

En principio, cualquier materia o curso es aceptable. Esto incluye todas las materias de grado de la UNSAM, de la UBA y de otras universidades. **Al fondo están las materias que algunos de nosotros ya cursamos! profit y llenen el formulario si ya cursaron alguna ;)**

Acá la UNSAM pone algunas: [www.unsam.edu.ar/oferta/carreras/materias.asp](http://www.unsam.edu.ar/oferta/carreras/materias.asp) pero no hay que limitarse a esa lista (por suerte).

Para biotecnología, hay algunos **cursos** que abren irregularmente sobre cultivo de células, genética de levaduras, qPCR, entre otros. La optativa para LBT es **Bioinformática**, que abre regularmente durante los segundos cuatrimestres. Otras carreras de la UNSAM ofrecen materias interesantes como **Toxicología** o **Bioprocesos.**

En la UBA hay un montón de materias interesantísimas para hacer. En el campus virtual de exactas pueden encontrar muchas de ellas, pero no se queden con esto y usen Google por favor. Ejemplo: materias del departamento de Química Biológica, 2015Q2 acá: [campus.exactas.uba.ar](http://campus.exactas.uba.ar/course/category.php?id=159).

Algunas interesantes para LBT pueden ser  **Virología Molecular** o Biología Molecular de Eucariotas Inferiores**, y siempre pueden mirar los [programas](https://exactas.uba.ar/ensenanza/carreras-de-grado/ciencias-biologicas/) de cada carrera.

Muchos institutos y facultades dan cursos cortos, pero intensivos, más especializados. El Leloir, UBA, INTA, IIB-INTECH, y tantos otros ofrecen de estos *cada tanto* y usualmente la información llega a través de mails y flyers. O sea, hay que estar atento y/o buscar bien.

## 🗃️ Aportes

Listado de materias optativas y experiencias de estudiantes de la UNSAM en el tema que nos envian por un google forms.

- [Abrir *Formulario*](http://docs.google.com/forms/viewform?hl=es&id=166hM357GoDPjPK1WhLUWj8JrjoopLlbCmDV2oEY_RMg)
- [Abrir Materias Optativas (respuestas)](https://docs.google.com//spreadsheets/d/1UaTNrLqJ3FdSZcNup0nElNHj-rt_1WM5oEwEeiP9VLA/edit?gid=1897932175)
