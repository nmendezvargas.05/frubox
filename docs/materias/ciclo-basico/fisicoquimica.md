# Fisicoquímica

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)


## 💭 Experiencias

Opinión hasta el 2016

A esta materia le pasa eso de tener docentes que son muy buenos y docentes que no son docentes en realidad. La **calidad** de las clases teóricas depende 99% en cuál de los dos te toca. La materia no es fácil.

-   Javier es un capo. Sabe mucho, explica mucho, es MUY detallado, y es paciente. Las evalucaiones requieren que comprendas bien los contenidos, y la dificultad va de la mano con el nivel de la clase.
Pueden compararlo con Gustavo Duffó de química orgánica 😊 es un docente muy dedicado.

-   Rinaldi no; todo lo contrario. Se equivoca, da las cosas demasiado por arriba, evalúa con criterios inaedecuados y corrige igual.
La fisicoquímica tiene temas básicos muy profundos para aportar, que en esta clase se pierden.

Pidan por Javier, o mejor aún, recen por él. Si FQ no es correlativa con nada más, como pasa con LBT, les diría que esperen a que vuelva Javier para cursarla o arriésguense a sufrir el horror.

Yo opino que es mejor estar a pleno bien y aprendiendo de un chabón que sabe mucho, a estar a pleno odiando las clases y estar a pleno mal porque te dan algo a medias y después te toman cualquier cosa.

Históricamente, los ayudantes son unos capos y te ayudan un montón siempre, más allá del docente de las teóricas.

Profit.

✏️ Esta materia tiene descripción breve y sesgada. Envianos más recomendaciones y opiniones sobre los temas, las clases y el cuerpo docente. Todo lo que pueda ayudar a los futuros alumnos a tener una mejor cursada, sirve. Escribinos con tu aporte a <unsam@frubox.com>.

## 📱 Grupos de whatsapp


💬 **-**

## 📂 Carpeta Frubox

***Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer)***

<iframe src="https://drive.google.com/embeddedfolderview?id=1Auzaf7YNL7ZYzSQHEAMC4H5cw1TaWw7s" style="width:100%; height:600px; border:0;"></iframe>
