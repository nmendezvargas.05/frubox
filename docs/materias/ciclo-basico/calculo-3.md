# Cálculo III

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)

🛠️ [Calculadora de derivadas](https://www.calculadora-de-derivadas.com/)

🛠️ [Calculadora de integrales](https://www.calculadora-de-integrales.com/)

## 💭 Experiencias

- Cursé la materia el primer cuatri de 2022 con Marcela Fabio y Marta. Les cuento lo que me sirvió hacer a mi, y lo que me hubiese gustado que me cuenten antes de arrancar. Para la cursada: todas las semanas después de cada tema nuevo trataba de avanzar lo más que podía con los ejercicios de cada tema (un poco de los de las guías y otro poco de los que daba Marta) para ir teniendo una idea del planteo. Después para los parciales, estudié de parciales viejos y la teoría la usé para terminar de entender y justificar bien cada desarrollo (y entender el contexto de los ejercicios). Si llegas a querer ir resumiendo la teoría, para el final sirve mucho. También me sirvieron algunos de los ejercicios que iba dando Marta (es medio clave, porque los parciales -al menos este cuatri- los armaba ella). Además, me ayudó mucho practicar la justificación de los ejercicios con otrxs compañerxs. Para el final (lo rendí en Julio de 2022): no estudié para nada parecido que para los parciales. Los finales los arma Marcela y son casi plenamente teóricos. La estructura que me tocó fue de V/F a justificar y un ejercicio práctico a desarrollar. Nos dió 1 hora entonces es muy importante practicar hacer ejercicios rápido y tener la teoría bastante pulida. Después si aprobabas el escrito, te llamaba a un oral donde te hace preguntas y te pide ejemplos a partir de la teoría de la materia (tipo funciones que cumplan condiciones determinadas de derivabilidad, de singularidades, etc.). Para la materia en general: no te rindas!!! A veces cuesta agarrarle la mano, pero insistiendo y practicando termina saliendo 😊.

✏️ Escribinos con tu aporte a <unsam@frubox.com> y lo agregamos acá.


## 📱 Grupos de whatsapp

💬 [Cálculo 3](https://chat.whatsapp.com/J5FbAvwFspm5zdAr5Q8Dzi)



## 📂 Carpeta Frubox

 ***Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer)***

<iframe src="https://drive.google.com/embeddedfolderview?id=1HWHu3fuxrKdQ5MNcmUFAbYy3KlkMBON4" style="width:100%; height:600px; border:0;"></iframe>
