# Ética y ejercicio profesional

???+ question "Esta materia no tiene descripción"

    Si tenés tiempo para escribir una descripción y pasar información relevante, podríamos publicar por acá:

    -   Opiniones, reseñas, comparaciones con otras carreras/universidades.
    -   Planes de estudio y recomendaciones para organizar los cuatrimestres.
    -   Respuestas a preguntas frecuentes o importantes: ¿Se puede cursar de noche? por ejemplo.

    Envianos recomendaciones y opiniones sobre los temas, las clases y el cuerpo docente. Todo lo que pueda ayudar a los futuros alumnos a tener una mejor cursada, sirve. Escribinos con tu aporte a <unsam@frubox.org>.

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)

## 📱 Grupos de whatsapp
💬[Etica y ejercicio profesional](https://chat.whatsapp.com/BXTPnPJ3UWt1Uy6AgyUSRJ)

## 📂 Carpeta Frubox 

<iframe src="https://drive.google.com/embeddedfolderview?id=1i-1mUqCvAtxTqvvjTvUny2CatfqwtQSe" style="width:100%; height:600px; border:0;"></iframe>
