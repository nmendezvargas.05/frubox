# Física III

📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)

## 💭 Experiencias

 Una materia para no pasarla mal, o lo mejor posible. La física es *como un salamín*, dicen, pero en realidad así es todo el aprendizaje, solo que acá te lo hacen bastante más grueso y grasoso... a propósito.

 Hay algunas cosas que se repiten hace bastante:

 1.  Si no aprobás los dos primeros parciales de una, no podés hacer los laboratorios en el horario que corresponde (debido a 3, supuestamente). Si aprobás la cursada, tenés prioridad para hacer los TP el cuatrimestre que sigue.
 2.  Cada examen dura 3 horas, parcial y final. Si sos un capo llegás bien, si te tenés que poner a pensar o a corregir algo, ya estás al horno.
 3.  No hay espacio en los laboratorios para la gente. Esto es un problema siempre, va más allá de quienes aprueben o no.
 4.  Los parciales tienen ejercicios que se repiten siempre casi igual, y otros problemas que no y hay que pensar. Practicar mucho con guías  y exámenes sirve para aprobar, pero no siempre es suficiente. Ni hablar de una buena nota.
 5.  Los alumnos "pueden" hacer el laboratorio en febrero si no lo hicieron durante el cuatrimestre. Sin embargo piden que sean al menos 10 (¿Por qué?) y lo hacen en época de finales.

 Algunos testimonios actualizados sobre la metodología de evaluación:

-   En el turno mañana con _**Diego Rubí**_ hay dos exámenes parciales (a mes y medio de cursada (guías 1 a 6) y primera semana del último mes de cursada (guías 7 a 10) aprox) y cuatro laboratorios. Hay una semi-promoción, si llegas a cierta nota se te toma un final corto teórico sobre los últimos dos temas explicados luego del segundo parcial.

-   En el turno noche con _**Francisco Parisi**_ hay cuatro exámenes parciales y tres laboratorios. Es final obligatorio, sin final corto. Los exámenes parciales consisten en:

+Dos exámenes diferidos, es decir, te lo dan un miércoles y lo entregas al próximo miércoles por el campus. Se aprueban con 7. +Dos exámenes presenciales de entrega inmediata. Se aprueban con 5. La materia se divide en dos partes, cada una consiste en un parcial diferido y uno presencial. Aproximadamente el primer parcial diferido es a las tres semanas de cursada (guias 1 a 3) y el primer parcial presencial al mes y medio de cursada (guías 4 a 6), el segundo parcial diferido a las tres-cuatro semanas del parcial presencial (guías 7-8 ) y en la primer semana del último mes de cursada es el segundo parcial presencial (guías 9-10).

-   En ambos turnos, luego del último parcial la cursada se extiende una semana más en la que se siguen explicando temas que entran al final. En el caso del turno mañana, estos son los temas que se les toma a la gente que llegó al final corto.

En el sitio oficial suben guías y exámenes anteriores; nada hecho por alumnos [http://www.fisicarecreativa.com/unsam_f3/](http://www.google.com/url?q=http%3A%2F%2Fwww.fisicarecreativa.com%2Funsam_f3%2F&sa=D&sntz=1&usg=AOvVaw3VGHKdZElr0ixsYIY4ouHM) pero para eso estamos nosotros. Profit!

Opiniones personales:

Esta materia tiene problemas sistémicos graves y la gente a cargo de la materia ya se acostumbró a tenerlos, demostrando falta de criterio y perseverancia para resolverlos y así educar mejor a los alumnos.

## 📱 Grupos de whatsapp

💬[Física III TM](https://chat.whatsapp.com/F5df3HdyLg4FEzjeYTSBVE)

💬[Física III TN](https://chat.whatsapp.com/JC14a0dPjUl4AHXQpD7EVk)





## 📂 Carpeta Frubox

***Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer)***

 

<iframe src="https://drive.google.com/embeddedfolderview?id=1TJPfsKrhU_54B81PK8en6_K9F8ggbXYj" style="width:100%; height:600px; border:0;"></iframe>
