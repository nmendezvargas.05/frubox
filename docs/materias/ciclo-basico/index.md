# Materias ECyT General

Wujuu pasé el CPU, ahora me la van a dar en [Introducción al Análisis Matemático](/materias/ciclo-basico/analisis-matematico)! O no... 😉

## 💡 ¡Bienvenido al ciclo básico de la unsam!

> Sé feliz y estudiá!

Para algunas carreras el promedio es hincha (para conseguir becas o pasantías por ejemplo), esta es una buena etapa para construirlo. Si va mal o no lográs ese buen promedio no desesperes, hay oportunidades para todo, a medida que vayas avanzando en la carrera van a aparecer. Además, en vez de angustiarse por el camino estándar, se siente mejor marcar el camino propio.

Para ayudar a palear todas las injusticias de un sistema educativo superpoblado y bancario, existe juntarte con tus amigos a estudiar y este sitio hermoso donde buscar y dejar material copado y útil.

Si sos copada/o lo que tengas mandalo a <unsam@frubox.org>, los futuros estudiantes te lo van a agradecer 😊.

**APUNTES, LIBROS, EXÁMENES ANTERIORES, GRUPOS DE WHATSAPP, CONSEJOS**

Vos lo enviás, nosotros le damos formato, te agradecemos y lo subimos.

Todo esto es gracias al esfuerzo de la comunidad estudiantil de la Unsam, con  **tu** 🫵 aporte podés ayudar a mucha gente.

