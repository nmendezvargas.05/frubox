# Agrobiotecnología

## 💡 Acerca de la materia

La ingeniería en Agrobiotecnología es una carrera única por al menos tres razones: 

1) Por la especificidad de conocimiento que transmite. Se trata de la única carrera de Argentina dedicada específicamente a la biotecnología agropecuaria, creada por la UNSAM en conjunto con el Instituto Tecnológico de Chascomús (INTECH) y el Instituto Nacional de Tecnología Agropecuaria (INTA). 

2) Por la forma de ingreso. Se distingue del resto de la oferta pública y privada por requerir a sus ingresantes que ya hayan aprobado el primer año de una carrera afín (agronomía, biología, biotecnología, tecnicatura universitaria de laboratorio, veterinaria, zootecnia, etc.). Cada año se abren 10 vacantes para el ingreso a la ingeniería, lo que permite una educación personalizada para cada alumno tanto en aspectos teóricos como en prácticos de laboratorio.

3) Por el diseño de la cursada. La carrera se divide en tres trayectos. 
Trayecto de Conocimientos Básicos, durante el primer año de la carrera. (Tópicos de Matemática, Tópicos de química, Tópicos de Física, Biología Vegetal, Biología Animal, etc.)
Trayecto de Orientación a la Biotecnología, entre el segundo y tercer año (Biología Molecular, Ingeniería Genética, Microbiología, Biofábricas, Emprendimiento Biotecnológicos, etc.)
Trayecto de Especialización en Agrobiotecnología, entre tercero y cuarto año (Clonación y Mejoramiento Animal, Mejoramiento Genético y Genómico, Bioinformática, Producción Vegetal, Producción Animal, etc.)
El tramo final de la carrera está compuesto por 6 materias optativas y la realización de una Tesis. 

Cada año se abren 10 vacantes para estudiantes que hayan completado un año de una carrera afín. Quienes resulten seleccionados reciben una beca de ayuda económica y alojamiento en ambas sedes de cursada.

Para consultas, buscanos en instagram como agrobiotec_unsam o via mail docencia@intech.gov.ar

[Plan de estudios](https://drive.google.com/file/d/1N1GH68EKHYkT-cLbihLLvkJg6v8ifb36/view?usp=drive_link>)


📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)

## 💭 Experiencias

✏️ Escribinos con tu aporte a <unsam@frubox.com> y lo agregamos acá.

## 📱 Grupos de whatsapp

💬 **-**

## 🗺️ Mapa de correlativas

**-**

## 📂 Carpeta Frubox

<iframe src="https://drive.google.com/embeddedfolderview?id=1azthCuZmCLZMZQG_jkz8JRZmwIYVcVho" style="width:100%; height:600px; border:0;"></iframe>

