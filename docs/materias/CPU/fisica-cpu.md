# Física CPU


## 💡 Cómo aprobar Física CPU y no morir en el intento

Hola, a vos que sos nuevo/a en la universidad o estás recursando...bienvenido/a!

???+ note
    Esta guía de recomendación es parecida (pero NO igual: atenti!) a las que hay para las otras materias


**¿Qué es lo nuevo de esta recomendación?**

- Antes teníamos armados unas "recomendaciones" generales sobre cada materia del CPU. Decidimos simplificarlo y re-escribirlas específicamente para cada materia, 10 "preguntas/respuestas", con el fin de hacerlo mas corto y directo al grano, para así poder ayudarte mejor.


 1) **En el secundario no aprendí nada de Física... ¿Estoy al horno?**

No, seguramente el/la que tuvo mejor base de la secundaria va a resultarle más fácil (o no, quién sabe). Te va a costar un poco agarrarle la mano a los ejercicios o saber de qué pepino está hablando el profe pero vas a poder. Tenele paciencia y TENETE paciencia pero por sobre todo no desesperes, venimos a la facu a aprender, si supiésemos todo no estaríamos estudiando. Estudía, practicá y preguntá todo sin vergüenza que nadie nace sabiendo.


2) **Física del CPU... ¿Es una materia difícil?**

La dificultad de una materia depende de cada uno/a, a algunos/as les cuesta más a otros/as menos, si no tuviste base en la secundaria es probable que sea un poco más pesada. No cuelgues los con los ejercicios, es una materia muy muy práctica. Tener la guía al día va a ser tu mejor herramienta.


3) **Me cuestan las ciencias exactas... ¿Estoy al horno?**

No, no estás al horno. 

La materia está preparada para recibir estudiantes con distintos niveles de habilidad y darles las herramientas necesarias para que se defiendan durante la carrera. Si te cuesta, puedes apoyarte en los profesores, que están para ayudarte. También hay clases de apoyo y bancos de apuntes. Nunca tengas miedo de preguntar o pedir ayuda con lo que más te cueste. Recordá que están todos en el mismo barco y no todos tienen las mismas facilidades. A veces cuesta más, toma más trabajo pero de la facultad se recibe el/la persistente. Estudia, practica y si sale mal… se intenta otra vez! No está muerto quien pelea.


4) **¿Puedo aprobar el segundo parcial de Física CPU sin saber los contenidos del primero?**

El segundo parcial es correlativo al primero. Es decir, desarrolla temas basados sobre lo aprendido en la primera parte de la materia, por lo que es necesario tener un conocimiento básico de los contenidos del primer parcial. Sin embargo, desaprobar el primer parcial no significa no poder aprobar el segundo. Quizá te faltó un poco para llegar a aprobar el primero, y terminas desarrollando ese conocimiento faltante mientras profundizas en los temas del segundo. Por lo que no es determinante, lo importante es no bajar los brazos y seguir focus en la materia.



5) **¿Cuál es el secreto para aprobar?**

- La constancia, aprueba el/la constante, se recibe el/la constante. 
- Hacer ejercicios es fundamental.
- Prepará los exámenes con tiempo, no los subestimes. Estudiá de parciales viejos.
- No procrastines. A veces la materia parece que va lenta, pero si te dormís te come. Llevala al día.




6) **Me toco un mal profesor de Física CPU y no le entiendo nada. ¿Qué hago?**

Relax, primero, fijate si te podés cambiar de grupo (esto no suele pasar porque los cupos suelen estar llenos pero con probar no se pierde nada). Segundo, aprovechá todo el material subido hay teóricas y ejercicios que te pueden servir de herramientas, preguntá mil veces si hace falta, preguntá a tus compañeros/as, preguntá  en [FruboxUnsam](https://www.google.com/url?q=https%3A%2F%2Fwww.facebook.com%2Fgroups%2Ffruboxunsam&sa=D&sntz=1&usg=AOvVaw1aXTZAFm5cZ5Wa9PMqcedR), Unsamers,[EECyT](https://www.google.com/url?q=https%3A%2F%2Fwww.facebook.com%2Fgroups%2Feecyt&sa=D&sntz=1&usg=AOvVaw2DJWNra0c1ahoZWXkzKabb).

Si te cuesta seguir al profesor en algun tema particular, recurrí a youtube como apoyo. Es una buena herramienta para repasar y profundizar en los temas que cuestan más.

7) **En resumen: Qué me recomendás para aprobar esta materia sí o sí?**

**Ponete a resolver parciales a full**. Si ya estuviste jugando un poco con la guía de ejercicios, sumale resolver parciales. La estructura de ejercicios es casi siempre la misma, los mismos ejercicios "clave" e "integradores" que evalúan varios contenidos al mismo tiempo. Esto aplica también cuando estés adentro de la carrera: parciales, parciales y más parciales. Igual no te confíes con solo resolver ejercicios de parciales viejos, la teoría hay que leerla siempre.

- Tené en cuenta que los recuperatorios caen todos juntos junto con el cierre de las materias y los segundos parciales, así que cuantos menos recuperatorios tengas que dar, mejor la vas a pasar. 😄


8) **Si ya me va mal en el CPU, una vez que este adentro de la carrera...¿Estoy perdido/a?**

No. No estas perdido/a. El CPU es el primer paso entre la secundaria y la facultad, el cambio de ritmo de estudio, la base que puedas traer o no de la secundaria y un montón de cosas están metidas en el medio y pueden joder. Si va mal en el cpu se recursa, se aprende y se pasa a lo siguiente, igual que cuando estés más avanzado/a en la carrera.

Es una etapa que te permite acercarte lentamente a la vida universitaria. A veces cuesta acostumbrarse, los tiempos no son los mismos para todos. ¡A no desesperar!


9) **¿Tenés un particular que me puedas recomendar?**


Preguntá en cualquiera de las páginas de la facu, Unsamers, [FruboxUnsam](https://www.google.com/url?q=https%3A%2F%2Fwww.facebook.com%2Fgroups%2Ffruboxunsam&sa=D&sntz=1&usg=AOvVaw1aXTZAFm5cZ5Wa9PMqcedR) o [Estudiantes CyT UNSAM](https://www.google.com/url?q=https%3A%2F%2Fwww.facebook.com%2Fgroups%2Feecyt&sa=D&sntz=1&usg=AOvVaw2DJWNra0c1ahoZWXkzKabb), y vas a encontrar.


10) **¿Y si no tengo plata para ir a un particular?**


No es obligatorio gastar plata para aprobar. Forma grupos de estudio (organizados en la facu o en los grupos de facebook). Son muy útiles y siempre va a haber un tema que tus compañeros/as entiendan mejor que vos y vice versa. Recordá: Dos cabezas (o más) piensan mejor que una. También en el grupo de facebook podes postear tus dudas sobre ejercicios (si les sacas una foto y la subís genial, así se entiende mejor).

Puede ser que en otras universidades no seas más que un número. Acá no: ¡Te queremos ayudar!


**Comentario final**

- Si en una de esas llegas a recursar alguna/s materia/s del CPU no pasa nada: estudia mas para la próxima y sacate mil. Lo importante es que no abandones!!!

 Estamos para darte una mano. No aflojes. Te va a ir bien.


 
## 💭 Experiencias

✏️ Escribinos con tu experiencia a <unsam@frubox.com> y lo agregamos acá.

## 📱 Grupos de whatsapp

💬 **-**

## 📂 Carpeta Frubox

**Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer)**

<iframe src="https://drive.google.com/embeddedfolderview?id=1u79IUqXmzrM9PJzqGDAzgqdDk0ypvuQj" style="width:100%; height:600px; border:0;"></iframe>
