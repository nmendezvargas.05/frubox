# IEU CPU

## 💡 Cómo aprobar IEU CPU y no morir en el intento

Hola, a vos que sos nuevo/a en la universidad o estas recursando...bienvenido/a!


???+ note
    Esta guía de recomendación es parecida (pero NO igual: atenti!) a las que hay para las otras materias


**Esta materia es un poco...¿densa?**
 
 Si no te gusta, claro. Pero bueno...la opinión es muy polarizada. Algunos/as la odian, otros/as la aman, depende mucho de la orientacion que tenga cada estudiante. Algunos estudiantes tienen predilección hacia las exactas, otros hacia sociales.

- Dale bola a la unidad numero 1 de lógica (proposiciones y toda la bola), SIEMPRE entra en los exámenes. El resto de las unidades (teóricas) las podes estudiar por tu cuenta, hacete resúmenes!

- En el archivo que se llama "Guía para el examen" vas a encontrar parciales tomados anteriormente, eso te va a servir como guía. Te recomiendo que te hagas resúmenes sobre los "conceptos" que aparecen en las guías (ej: diferencia entre ciencia fáctica y formal) ya que SIEMPRE toman esas definiciones y que te hagas resúmenes de los textos que te dan para leer (o busca resúmenes en internet para vos que no tenes tiempo...hay algunos muy bien explicados).


**Ponete a resolver parciales a full**

 - Si ya estuviste jugando un poco con la guía de ejercicios, sumale resolver parciales. La estructura de ejercicios es casi siempre la misma, los mismos ejercicios "clave" e "integradores" que evalúan varios contenidos al mismo tiempo. Esto aplica también cuando estés adentro de la carrera: parciales, parciales y más parciales. Igual no te confíes con solo resolver ejercicios de parciales viejos, la teoría hay que leerla siempre.

- Tené en cuenta que los recuperatorios caen todos juntos junto con el cierre de las materias y los segundos parciales, así que cuantos menos recuperatorios tengas que dar, mejor la vas a pasar. 😄

- No subestimes a la materia, que es lo que muchos hacen y después les va mal.

**En resumen**

- IEU es saber hacer ejercicios de lógica y estudiarse los conceptos/definiciones que aparecen en los textos (ej, diferencia entre cs fácticas y formales, definición de ciencia pura, ciencia aplicada y tecnología, etc) ya que ambas cosas SIEMPRE entran en el examen. Con las definiciones y la parte de lógica ya lo tenes casi cocinado, y con unos buenos resúmenes la rompes.

- También suele entrar alguna que otra pregunta sobre alguna parte especifica de algún texto...(ej: ¿qué quiso decir Aristóteles cuando se refirió a....?) Así que si tenés un buen resumen (tuyo o de alguien) mejor.

- El cuadrito de las diferencias de Conocimiento científico vs Sentido común (esta en el archivo de word) tenelo bien a mano y estudiátelo porque SIEMPRE lo toman (o la grannn mayoría de las veces)

- Si en una de esas llegas a recursar alguna/s materia/s no pasa nada: estudia mas para la próxima y sacate mil. Lo importante es que no abandones!


## 💭 Experiencias

✏️ Escribinos con tu experiencia a <unsam@frubox.com> y lo agregamos acá.

## 📱 Grupos de whatsapp

💬 **-**

## 📂 Carpeta Frubox

**Espera un ratito a que cargue la carpeta del Google Drive... (demora unos segundos en aparecer)**

<iframe src=https://drive.google.com/embeddedfolderview?id=1S8ubPtuuP5F1RytkTwhw0NccrR457pc- style="width:100%; height:600px; border:0;"></iframe>
