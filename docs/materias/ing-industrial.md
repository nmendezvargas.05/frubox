# Industrial

## 💡 Acerca de la materia

Ingeniería Industrial tiene su propio [grupo de facebook](https://www.google.com/url?q=https%3A%2F%2Fwww.facebook.com%2Fgroups%2F271118403092142%2F&sa=D&sntz=1&usg=AOvVaw26Djzafgh_B9rQBLYpYVpf), con muchos aportes.

Enlazamos acá la carpeta que organizaron: <https://drive.google.com/folderview?id=0B3J3wRZWAHgcVU1Cd0hPbDBrMG8>

???+ warning
    Es posible que debas pedir acceso a lxs admin de ese espacio.
    
    Si querés enviar material a <unsam@frubox.org> lo subiremos acá 😊


📚 Encontrá los libros que necesitás en [Libros y Bibliotecas](../../../biblioteca)

## 💭 Experiencias

✏️ Escribinos con tu experiencia a <unsam@frubox.com> y lo agregamos acá.

## 📱 Grupos de whatsapp

💬 **-**

## 🗺️ Mapa de correlativas
 **-**