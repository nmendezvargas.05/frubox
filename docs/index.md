---
title: Página principal
---

<span style="color:rgb(255,0,0);font-family:Arial;font-size:96px;font-weight:bold;">F</span><span style="color:rgb(255,153,0);font-family:Arial;font-size:96px;font-weight:bold;">r</span><span style="color:rgb(255,217,102);font-family:Arial;font-size:96px;font-weight:bold;">μ</span><span style="color:rgb(106,168,79);font-family:Arial;font-size:96px;font-weight:bold;">β</span><span style="color:rgb(61,133,198);font-family:Arial;font-size:96px;font-weight:bold;">o</span><span style="color:rgb(153,0,255);font-family:Arial;font-size:96px;font-weight:bold;">χ</span><span style="color:rgb(255,0,255);font-family:Arial;font-size:96px;font-weight:bold;">!</span>

Bienvenida al repositorio colaborativo e independiente de material de estudio para la UNSAM.

[📝 Material](/materias){ .md-button }
[📚 Biblioteca](/biblioteca){ .md-button }
[✉️ Aportar](mailto:unsam@frubox.org?subject=Aporte){ .md-button .md-button--primary }

Enlaces útiles:

-   Enviá tus aportes por mail a: <unsam@frubox.org>
-   Repositorio de apuntes en [materias](materias).
-   Instagram: [@frubox](https://instagram.com/frubox/)
-   Facebook: [grupo](https://www.facebook.com/groups/fruboxunsam) & [página](https://www.facebook.com/fruboxunsam/).

:tomato: :lemon: :watermelon: :tangerine: :grapes: :avocado: :hot_pepper: :cherries: :pineapple: :kiwi: :strawberry: :apple: :blueberries: :coconut:

# Bienvenida

Este sitio es un espacio para todas las fruboxeadas. Basada en MkDocs (Material theme) hecha con :heart: y GitLab Pages. ¿Querés editar o aportar? [Escribinos!](mailto:unsam@frubox.org)

Sitios principales:

- [Materias](/materias)
- [Biblioteca](/frubox/biblioteca)
- [Emails y Teléfonos](/frubox/contactos)

> Copate y escribinos a <unsam@frubox.org> con tus aportes :)

# Materias UNSAM

Listado completo en [materias](materias).

> Si querés ayudar a armar este sitio web, escribinos!

# Materias externas

Material de materias (?) de otras universidades:

-   El Drive de la ComBi (Biología UBA): <https://www.facebook.com/groups/954672261255212/about/>

# Sobre Frubox

¿Qué pensamos de lo que hacemos?

#### Propósito

La idea es compartir material para estudiar en la universidad, armando sitios que ayuen a entender más fácilmente cada materia que cursamos y tema que vemos. Esperamos que sea más útil para las primeras materias, y un lugar de referencia para las más avanzadas.

#### Hecha por todxs

Invitamos a **docentes**, **estudiantes** y **graduadxs** a editar y sugerir cambios para las páginas.

Quienes deseen editar libremente, sin pasar por revisión ni otras restricciones, pueden solicitar un usuario escribiendo a <unsam@frubox.org>.

Recibimos toda clase de solicitud, sugerencia o crítica.

#### Enlaces a otros sitios

Encontrá más material en el [sitio web oficial](http://frubox.org) de Frubox y, para todo lo demás, está el [grupo de Facebook](https://www.facebook.com/groups/fruboxunsam/).

#### Leénos

En instagram: [@frubox](https://instagram.com/frubox/)

---

![frubox_bannercito.png](./img/frubox_bannercito.png)
